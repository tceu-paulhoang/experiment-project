# experiment-project

Project for doing experiments in.

### Setup

clone with:
```
git clone git@gitlab.com:paulhoang-tceu/experiment-project.git
```

create a virtual environment for python with. (this will create an environment where dependencies can live and
 not be interfered with by other projects)

```
python3 -m venv venv
```

activate the local virtual environment

```
source ./venv/bin/activate      
```

install dependencies with:
```
pip3 install -r requirements.txt
```

any changes to the dependencies will need to have the requirements.txt file updated. Copy the output from the following into the requirements.txt file
```
pip3 freeze -l
```