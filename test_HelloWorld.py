import HelloWorld


def test_returnANumberShouldReturn42():
    assert HelloWorld.returnANumber() == 42
