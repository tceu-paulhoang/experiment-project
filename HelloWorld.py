# import sys
# sys.path.insert(0, 'venv/lib/python3.7/site-packages')
import requests


def returnANumber(event=None, context=None) -> int:
    return 42


def echoUrl(event=None, context=None) -> str:
    response = requests.get('https://postman-echo.com/get?foo1=bar1')
    return response.content

def clock(event=None, context=None) -> str:
    response = requests.get('https://1823dfef694d13438e2e34220c332b27.m.pipedream.net')
    return response.content


def main():
    print("Hello world")
    print("Getting the number %d" % returnANumber())
    print("Getting an echo back %s" % echoUrl())


if __name__ == "__main__":
    print("Running as local")
    main()
else:
    print("Running in a Paas env")
